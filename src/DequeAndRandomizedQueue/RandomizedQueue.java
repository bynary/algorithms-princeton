/*------------------------------------------------------------------------------
 *  Class to model a RandomizedQueue
 *  spec - http://coursera.cs.princeton.edu/algs4/assignments/queues.html
 *
 *  @author          Yuriy Bolotnyy
 *  Email:           yuriy.bolotnyy@gmail.com
 *  @version         1.0
 *  Written:         10/10/2017
 *  Last updated:    10/14/2017
 *
 *  Dependencies:    edu.princeton.cs.algs4
 *                   (i.e. http://algs4.cs.princeton.edu/code/algs4.jar)
 *  Compilation:
 *  Execution:
 *
 *----------------------------------------------------------------------------*/

import edu.princeton.cs.algs4.StdRandom;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class RandomizedQueue<Item> implements Iterable<Item> {
    private static final int INITIAL_CAPACITY = 1;
    private static final int UPSIZE_THRESHOLD = 1;
    private static final double DOWNSIZE_THRESHOLD = (double) 0.25;

    private Item[] q;
    private int n;
    private int lastLoc;
    // private int startLoc = 9;

    public RandomizedQueue()
    {
        q = (Item[]) new Object[2];
        n = 0;
        lastLoc = 0;
    }

    public boolean isEmpty()
    {
        return n == 0;
    }

    public int size()
    {
        return n;
    }

    private void resize(int capacity)
    {
        Item[] copy = (Item[]) new Object[capacity];
        for(int i = 0; i < n; i++)
        {
            copy[i] = q[i];
        }
        q = copy;
    }

    public void enqueue(Item item)
    {
        if(item == null) {
            throw new java.lang.IllegalArgumentException();
        }
        if( n == q.length)
        {
            resize(2 * q.length);
        }
        q[n++] = item;
        lastLoc = n;
    }

    public Item dequeue()
    {
        if(size() == 0) {
            throw new NoSuchElementException();
        }
        int rand = StdRandom.uniform(n);
        Item value = q[rand];
        //q[rand] = null;
        if(rand != n -1) {q[rand] = q[n -1];}
        q[n -1] = null;
        n--;
        if(n > 0 && n <= q.length/4)
        {
            resize(q.length/2);
        }
        return value;
    }

    public Item sample()
    {
        if(size() == 0)
        {
            throw new NoSuchElementException();
        }
        int rand = StdRandom.uniform(n);
        Item value = q[rand];
        if(n > 0 && n == q.length/4)
        {
            resize(q.length/2);
        }
        return value;
    }

//    private void findStartLoc()
//    {
//
//    }

    public Iterator<Item> iterator()
    {
        return new RandomIterator();
    }

    private class RandomIterator implements Iterator<Item>
    {
        private int randLoc = 0;
        // private int covered = 0;
        private int copySize = n;
        private Item[] copy = (Item[]) new Object[copySize];

        private RandomIterator()
        {
            for(int i = 0; i<copySize;i++)
            {
                copy[i] = q[i];
            }
        }

        public boolean hasNext()
        {
            return copySize > 0;
        }

        public void remove()
        {
            throw new UnsupportedOperationException();
        }

        public Item next()
        {
            if(copySize == 0)
            {
                throw new NoSuchElementException();
            }
            randLoc = StdRandom.uniform(copySize);
            Item currentItem = copy[randLoc];
            if(randLoc != copySize-1)
            {
                copy[randLoc] = copy[copySize-1];
            }
            copy[copySize-1] = null;
            copySize--;
            return currentItem;
        }
    }
}
