/*------------------------------------------------------------------------------
 *  Class to test Randomized queue
 *  spec - http://coursera.cs.princeton.edu/algs4/assignments/queues.html
 *
 *  @author          Yuriy Bolotnyy
 *  Email:           yuriy.bolotnyy@gmail.com
 *  @version         1.0
 *  Written:         10/10/2017
 *  Last updated:    10/14/2017
 *
 *  Dependencies:    edu.princeton.cs.algs4
 *                   (i.e. http://algs4.cs.princeton.edu/code/algs4.jar)
 *  Compilation:
 *  Execution:
 *
 *----------------------------------------------------------------------------*/

import edu.princeton.cs.algs4.StdOut;

public class Permutation {

    public static void main(String[] args) {
        RandomizedQueue<String> q;
        int k;

        if (args.length == 0) {
            return;
        }

        k = -1;
        k = Integer.parseInt(args[0]);
        if (k == -1) {
            throw new IllegalArgumentException();
        }

        q = new RandomizedQueue<String>();
        for (int i = 1; i < args.length; i++) {
            q.enqueue(args[i]);
        }

        int i = 1;
        while (i <= k) {
            StdOut.println(q.sample());
            i++;
        }
    }
}
