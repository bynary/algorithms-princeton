/*------------------------------------------------------------------------------
 *  Class to model a Double Ended Queue
 *  spec - http://coursera.cs.princeton.edu/algs4/assignments/queues.html
 *
 *  @author          Yuriy Bolotnyy
 *  Email:           yuriy.bolotnyy@gmail.com
 *  @version         1.0
 *  Written:         10/10/2017
 *  Last updated:    10/14/2017
 *
 *  Dependencies:    edu.princeton.cs.algs4
 *                   (i.e. http://algs4.cs.princeton.edu/code/algs4.jar)
 *  Compilation:
 *  Execution:
 *
 *----------------------------------------------------------------------------*/

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Deque<Item> implements Iterable<Item> {
        private Node first;
        private Node last;
        private int size;

        private class Node {
            Item item;
            Node next;
            Node previous;

            public Node(Item item) {
                this.item = item;
                next = null;
                previous = null;
            }

            public boolean hasNext() {
                return next != null;
            }

            public boolean hasPrevious() {
                return previous != null;
            }
        }

    // construct an empty deque
    public Deque() {
        first = null;
        last = first;
        size = 0;
    }

    // is the deque empty?
    public boolean isEmpty() {
        return (first == null);
    }

    // return the number of items on the deque
    public int size() {
        return size;
    }

    // add the item to the front
    public void addFirst(Item item) {
        checkIllegalArgument(item);

        Node newFisrt = new Node(item);

        if (isEmpty()) {
            // added element will become the first and last in one face
            first = newFisrt;
            last = first;
        }
        else {
            // insert on top of first
            Node oldFirst = first;

            newFisrt.next = oldFirst;
            oldFirst.previous = newFisrt;

            first = newFisrt;
        }
        size++;
    }

    // add the item to the end
    public void addLast(Item item) {
        checkIllegalArgument(item);

        Node newLast = new Node(item);

        if (isEmpty()) {
            // added element will become the first and last in one face
            last = newLast;
            first = last;
        }
        else {
            // insert after last
            Node oldLast = last;

            newLast.previous = oldLast;
            oldLast.next = newLast;

            last = newLast;
        }
        size++;
    }

    // remove and return the item from the front
    public Item removeFirst() {
        checkNoSuchElement();

        Item item = first.item;

        if (size == 1) {
            // queue will become empty after remove first
            first = null;
            last = first;
            size = 0;
        }
        else {
            Node newFirst = first.next;
            newFirst.previous = null;
            first = newFirst;
            size--;
        }

        return item;
    }

    // remove and return the item from the end
    public Item removeLast() {
        checkNoSuchElement();

        Item item = last.item;

        if (size == 1) {
            // queue will become empty after remove first
            last = null;
            first = last;
            size = 0;
        }
        else {
            Node newLast = last.previous;
            newLast.next = null;
            last = newLast;
            size--;
        }

        return item;
    }

    // return an iterator over items in order from front to end
    public Iterator<Item> iterator() {
        return new ListIterator();
    }

    private class ListIterator implements Iterator<Item> {
        private Node current;

        public ListIterator() {
            current = first;
        }

        public Item next() {
            if (current == null) throw new NoSuchElementException();

            Item item = current.item;

            if (hasNext()) {
                current = current.next;
            }

            return item;
        }

        public boolean hasNext() {
            return current != null;
        }

        public void remove() {
            throw new java.lang.UnsupportedOperationException(
                    "'remove() operation is not supported");
        }
    }

    private void checkIllegalArgument(Item item) {
        if (item == null) throw new IllegalArgumentException(
                "Argument 'item' should not be null");
    }

    private void checkNoSuchElement() {
        if (isEmpty()) throw new NoSuchElementException(
                "Can not do action on an empty queue");
    }
}
