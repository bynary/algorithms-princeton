import edu.princeton.cs.algs4.WeightedQuickUnionUF;

public class Helper {
    public static void print2Darray(int[][] a, int startIndex) {
        int rows = a.length;
        int cols = a[0].length;
        int fieldWidth = 3;

        System.out.printf("%" + fieldWidth + "s", "");
        for (int col = startIndex; col < cols; col++) {
            System.out.printf("x%d", col);
            System.out.printf("%" + (fieldWidth - 2) + "s", "");
        }

        for (int row = startIndex; row < rows; row++) {
            System.out.printf("%ny%d", row);

            for (int col = startIndex; col < cols; col++) {
                System.out.printf("%" + fieldWidth + "d", a[row][col]);
            }
        }
    }

    public static void print2Darray(int[][] a, Percolation p) {
        int startIndex = 1;
        int rows = a.length;
        int cols = a[0].length;
        int fieldWidth = 3;

        System.out.printf("%n%n%" + fieldWidth + "s", "");
        for (int col = startIndex; col < cols; col++) {
            System.out.printf("x%d", col);
            System.out.printf("%" + (fieldWidth - 2) + "s", "");
        }

        for (int row = startIndex; row < rows; row++) {
            System.out.printf("%ny%d", row);

            for (int col = startIndex; col < cols; col++) {
                if (p.isFull(row, col)) {
                    System.out.printf("%" + fieldWidth + "s", "*");
                } else {
                    System.out.printf("%" + fieldWidth + "d", a[row][col]);
                }
            }
        }
    }

    public static void printUF(WeightedQuickUnionUF uf, int gridSize) {
        int fieldWidth = 3;  // field width used as argument in prinf()
        int size = 1 + gridSize * gridSize + 1;
        int leftBracketPos = 1;
        int rightBracketPos = size - 1;

        System.out.printf("%n%nUF data-structure counts %d elements: ", size);

        System.out.printf("%n   i ");
        for (int i = 0; i < size; i++) {
            printBracketWhenNeeded(i, leftBracketPos, rightBracketPos);
            System.out.printf("%" + fieldWidth + "d", i);
        }

        System.out.printf("%nid[i]");
        for (int i = 0; i < size; i++) {
            printBracketWhenNeeded(i, leftBracketPos, rightBracketPos);
            System.out.printf("%" + fieldWidth + "d", uf.find(i));
        }

/*        System.out.printf("%nvirtual top: i%d <- [%d]", 0, uf.find(0));
        System.out.printf("%nvirtual bottom: i%d <- [%d]", size - 1, uf.find(size - 1));*/
    }

    private static void printBracketWhenNeeded(int n, int leftBracketPos, int rightBracketPos) {
        char leftBracket = '[';
        char rightBracket = ']';

        if (n == leftBracketPos) {
            System.out.print(" [");
        } else if (n == rightBracketPos) {
            System.out.print(" ]");
        }
    }
}
