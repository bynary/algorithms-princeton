/*------------------------------------------------------------------------------
 *  Program to estimate the value of the percolation threshold via Monte Carlo
 *  simulation.
 *
 *  @author          Yuriy Bolotnyy
 *  Email:           yuriy.bolotnyy@gmail.com
 *  @version         1.0
 *  Written:         10/07/2017
 *  Last updated:    10/08/2017
 *
 *  Dependencies:    edu.princeton.cs.algs4 (i.e. algs4.jar)
 *  Compilation:     javac PercolationStats.java
 *  Execution:       java PercolationStats 200 100
 *
 *  %java PercolationStats 200 100
 *  mean                     = 0.5930722500000003
 *  stddev                   = 0.0084030815167576
 *  95% confidence interval  = [0.5914252460227158, 0.5947192539772848]
 *
 *----------------------------------------------------------------------------*/

import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;

public class PercolationStats {
    private static final double CONFIDENCE_95 = 1.96;

    private final double[] iterations;
    private final int trialSize;
    private final double mean;

    // N is the size of the grid and T is the number of trials that will be
    // run. For each trial, we will calculate the number of sites that needed
    // to be opened for percolation.
    public PercolationStats(int N, int T) {
        int count;
        int totalSites;

        validateArguments(N, T);
        totalSites = N * N;
        trialSize = T;
        iterations = new double[T];

        // perform T independent computational experiments on an N-by-N grid
        for (int i = 0; i < T; i++) {
            count = 0;
            Percolation perc = new Percolation(N);

            while (!perc.percolates()) {
                // open some random site
                int col = StdRandom.uniform(1, N + 1);
                int row = StdRandom.uniform(1, N + 1);
                if (!perc.isOpen(col, row)) {
                    count++;
                }
                perc.open(col, row);
            }

            iterations[i] = (double) count / (double) totalSites;
        }

        this.mean = mean();
    }

    // sample mean of percolation threshold
    public double mean() {
        return StdStats.mean(iterations);
    }

    // sample standard deviation of percolation threshold
    public double stddev() {
        return StdStats.stddev(iterations);
    }

    // returns lower bound of confidence interval
    public double confidenceLo() {
        return mean - (CONFIDENCE_95 * stddev() / Math.sqrt((double)
                trialSize));
    }

    // returns upper bound of confidence interval
    public double confidenceHi() {
        return mean + (CONFIDENCE_95 * stddev() / Math.sqrt((double)
                trialSize));
    }

    private void validateArguments(int N, int T) {
        if (N <= 0 || T <= 0) {
            throw new java.lang.IllegalArgumentException("input needs to be " +
                    "positive values");
        }
    }

    // test client, called with the N and T. Where N is the size of the grid
    // and T is the number of trials that will be run.
    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);
        int t = Integer.parseInt(args[1]);

        PercolationStats ps = new PercolationStats(n, t);

        System.out.printf("mean\t\t\t\t\t = %.16f%n", ps.mean);
        System.out.printf("stddev\t\t\t\t\t = %.16f%n", ps.stddev());
        System.out.printf("95%% confidence interval\t = [%.16f, %.16f]",
                ps.confidenceLo(), ps.confidenceHi());
    }
}

