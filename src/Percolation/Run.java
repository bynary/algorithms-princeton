public class Run {
    public static void main(String[] args) {
        Percolation p;

        p = new Percolation(4);
        p.open(4, 1);
        p.open(2, 3);
        p.open(2, 2);
        p.open(1, 3);
        p.open(2, 1);
        openSiteAndCheck(3, 1, p);

        p = new Percolation(3);
        p.open(2, 2);
        p.open(1, 2);
        openSiteAndCheck(3, 1, p);

        p = new Percolation(3);
        p.open(2, 1);
        p.open(1, 3);
        p.open(2, 2);
        p.open(2, 3);
        openSiteAndCheck(3, 1, p);

        p = new Percolation(3);
        p.open(1,3);
        p.open(2,3);
        p.open(2,2);
        p.open(2,1);
        openSiteAndCheck(3,1, p);

        p = new Percolation(6);
        p.open(2, 1);
        p.open(5, 1);
        p.open(3, 2);
        p.open(4, 3);
        p.open(3, 3);
        p.open(5, 3);
        p.open(2, 2);
        p.open(5, 2);
        p.open(1, 1);
        openSiteAndCheck(6, 1, p);

        p = new Percolation(6);
        p.open(2, 6);
        p.open(3, 5);
        p.open(2, 5);
        p.open(4, 3);
        p.open(5, 3);
        p.open(1, 6);
        p.open(6, 2);
        p.open(5, 2);
        p.open(5, 1);
        p.open(3, 3);
        p.open(3, 4);
        p.open(6, 3);
        openSiteAndCheck(6, 1, p);

        p = new Percolation(1);
        openSiteAndCheck(1, 1, p);
    }

    private static void openSiteAndCheck(int row, int col, Percolation p) {
        p.open(row, col);
        System.out.printf("%n%n >> Open [%d, %d].    Percolates? %b%n", row,
                col, p.percolates());
        //Helper.print2Darray(p.grid, 1);
        //Helper.print2Darray(p.grid, p);
        //Helper.printUF(p.uf, p.gridSize);
    }
}
