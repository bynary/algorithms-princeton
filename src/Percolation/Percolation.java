/*------------------------------------------------------------------------------
 *  Class to model a percolation system
 *  spec - http://coursera.cs.princeton.edu/algs4/assignments/percolation.html
 *
 *  @author          Yuriy Bolotnyy
 *  Email:           yuriy.bolotnyy@gmail.com
 *  @version         1.0
 *  Written:         10/07/2017
 *  Last updated:    10/09/2017
 *
 *  Dependencies:    edu.princeton.cs.algs4
 *   *               (i.e. http://algs4.cs.princeton.edu/code/algs4.jar)
 *  Compilation:     javac Percolation.java
 *  Execution:       java Percolation
 *
 *----------------------------------------------------------------------------*/

import edu.princeton.cs.algs4.WeightedQuickUnionUF;

public class Percolation {
    private static final int TOP = 0;

    private boolean[][] opened;
    private final int bottom;
    private final int size;

    private int numberOfOpenSites;
    private final WeightedQuickUnionUF uf;

    /**
     * Creates N-by-N grid, with all sites blocked.
     */
    public Percolation(int n) {
        size = n;
        validateArgument(n);
        bottom = TOP + size * size + 1;
        uf = new WeightedQuickUnionUF(1 + size * size + 1);
        opened = new boolean[size][size];
        numberOfOpenSites = 0;
    }

    /**
     * Opens site (row, column) if it is not already.
     */
    public void open(int row, int col) {
        validateArguments(row, col);

        opened[row - 1][col - 1] = true;
        numberOfOpenSites++;

        if (row == 1) {
            uf.union(getQFIndex(row, col), TOP);
        }
        if (row == size) {
            uf.union(getQFIndex(row, col), bottom);
        }

        if (col > 1 && isOpen(row, col - 1)) {
            uf.union(getQFIndex(row, col), getQFIndex(row, col - 1));
        }
        if (col < size && isOpen(row, col + 1)) {
            uf.union(getQFIndex(row, col), getQFIndex(row, col + 1));
        }
        if (row > 1 && isOpen(row - 1, col)) {
            uf.union(getQFIndex(row, col), getQFIndex(row - 1, col));
        }
        if (row < size && isOpen(row + 1, col)) {
            uf.union(getQFIndex(row, col), getQFIndex(row + 1, col));
        }
    }

    /**
     * Is site (row, column) open?
     */
    public boolean isOpen(int row, int col) {
        validateArguments(row, col);
        return opened[row - 1][col - 1];
    }

    /**
     * Is site (row, column) full?
     */
    public boolean isFull(int row, int col) {
        validateArguments(row, col);
        return uf.connected(TOP, getQFIndex(row, col));
    }

    /**
     * Does the system percolate?
     */
    public boolean percolates() {
        return uf.connected(TOP, bottom);
    }

    /**
     * Get number of open sites
     */
    public int numberOfOpenSites() {
        return numberOfOpenSites;
    }

    private int getQFIndex(int row, int col) {
        validateArguments(row, col);
        return TOP + size * (row - 1) + col;
    }

    private boolean validArgument(int eitherRowOrCol) {
        return (0 < eitherRowOrCol && eitherRowOrCol <= size);
    }

    private void validateArgument(int n) {
        if (!validArgument(n)) {
            throw new java.lang.IllegalArgumentException(
                    "Argument should be >0 AND <= size"
            );
        }
    }

    private void validateArguments(int row, int col) {
        validateArgument(row);
        validateArgument(col);
    }
}
