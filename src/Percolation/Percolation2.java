/*------------------------------------------------------------------------------
 *  Class to model a percolation system
 *
 *  @author          Yuriy Bolotnyy
 *  Email:           yuriy.bolotnyy@gmail.com
 *  @version         1.0
 *  Written:         10/07/2017
 *  Last updated:    10/08/2017
 *
 *  Dependencies:    edu.princeton.cs.algs4 (i.e. algs4.jar)
 *  Compilation:     javac Percolation.java
 *  Execution:       java Percolation
 *
 *----------------------------------------------------------------------------*/

import edu.princeton.cs.algs4.WeightedQuickUnionUF;

import java.util.LinkedList;
import java.util.List;
/*
public class Percolation2 {
    // represents empty emptyOpenSite site (not connected to top)
    private static final int EMPTY_OPEN_SITE = 1;
    // represents 'blockedSite/closed' site
    private static final int BLOCKED_SITE = 0;
    private static final int EXTRA_ROW = 1;
    private static final int EXTRA_COLUMN = 1;

    private int numberOfOpenSites;
    private int[][] grid;   // n-by-n grid holder
    private final int gridSize; // represents n for n-by-n grid

    // data structure
    private final int virtualTop1Dindex;  // create virtual top site and virtual
    // bottom site
    private final int virtualBottom1Dindex;
    private WeightedQuickUnionUF uf; // represents WeightedQuickUnionUF 1D

    *//**
     * Initializes n-by-n grid, with all sites blocked
     *
     * @param n N size of N x N square grid.
     *//*
    public Percolation2(int n) {

        // Performance requirements. The constructor should take time
        // proportional to n2; all methods should take constant time plus a
        // constant number of calls to the union–find methods union(),
        // find(), connected(), and count().

        this.gridSize = n;

        // It's OK to use an extra row and column to deal with the 1-based
        // indexing of the percolation grid.
        int rows = EXTRA_ROW + n;
        int cols = EXTRA_COLUMN + n;

        // To fit n-by-n 2D grid to 1D aray, I need a space equal to n * n
        // plus one place before it for virtual top plus one place after grid
        // space for virtual bottom.
        this.virtualTop1Dindex = 0;

        // Since we have an extra row and extra column to deal with the
        // 1-based indexing, it need to be compensated.
        this.virtualBottom1Dindex = virtualTop1Dindex + (rows - EXTRA_ROW) *
                (cols - EXTRA_COLUMN) + 1;

        if (argumentIsIllegal(n)) return;

        this.grid = new int[rows][cols];

        // Initialize all sites to be blockedSite.
        for (int row = 1; row < rows; row++) {
            for (int col = 1; col < cols; col++) {
                grid[row][col] = BLOCKED_SITE;
            }
        }


        // Initialize WeightedQuickUnionUF 1D data structure to represent
        // 2D grid values.
        uf = new WeightedQuickUnionUF(virtualBottom1Dindex -
                virtualTop1Dindex + 1);

        // Connect entire first grid row to the virtual top.
        for (int col = 1; col < cols; col++) {
            int row = 1;
            int indexInUF = xyTo1D(row, col);
            uf.union(virtualTop1Dindex, indexInUF);
        }

        // Connect entire last grid row to the virtual bottom.
        for (int col = 1; col < cols; col++) {
            int row = gridSize;
            int indexInUF = xyTo1D(row, col);
            uf.union(virtualBottom1Dindex, indexInUF);
        }
    }


    *//**
     * Open site (row, col) if it is not open already
     *
     * @param row
     * @param col
     *//*
    public void open(int row, int col) {
        if (argumentIsIllegal(row) || argumentIsIllegal(col)) return;

        if (!isOpen(row, col)) {
            // It's closed, so open it.
            grid[row][col] = EMPTY_OPEN_SITE;
            numberOfOpenSites++;
            int currentSiteUFindex = xyTo1D(row, col);

            // Reflect 'emptyOpenSite' action in 'union' data structure.
            // Check all neighbors in +1 radius - if neigbor isFull or isOpen,
            // then connect neighbor and this site.

            for (PointRowColumn neighbor : getNeighbors(row, col)) {
                int r = neighbor.row;
                int c = neighbor.col;
                int neighborUFindex = xyTo1D(r, c);

                if (isFull(r, c) || isOpen(r, c)) {
                    uf.union(currentSiteUFindex, neighborUFindex);
                }
            }
        }
    }


    *//**
     * is site (row, col) open?
     *
     * @param row
     * @param col
     * @return True if site (row, col) is open
     *//*
    public boolean isOpen(int row, int col) {
        if (argumentIsIllegal(row) || argumentIsIllegal(col)) return false;

        return grid[row][col] == EMPTY_OPEN_SITE;
    }


    *//**
     * is site (row, col) full?
     *
     * @param row
     * @param col
     * @return True if site is open and connected to top
     *//*
    public boolean isFull(int row, int col) {
        if (argumentIsIllegal(row) || argumentIsIllegal(col)) return false;

        // check if the site is open and connected to top
        int indexUF = xyTo1D(row, col);
        return (grid[row][col] == EMPTY_OPEN_SITE
                && uf.connected(virtualTop1Dindex, indexUF));
    }


    *//**
     * Number of open sites
     *
     * @return number of open sites
     *//*
    public int numberOfOpenSites() {
        return numberOfOpenSites;
    }


    *//**
     * does the system percolate?
     *
     * @return True iff any site on bottom grid row is connected to
     * site on top row.
     *//*
    public boolean percolates() {
        // System percolates iff any site on bottom grid row is connected to
        // site on top row. That is equal to iff virtual top and virtual
        // bottom sites are connected.
        return uf.connected(virtualTop1Dindex, virtualBottom1Dindex);
    }

    *//**
     * Private method for uniquely mapping 2D coordinates to 1D
     * coordinates.
     *
     * @param row
     * @param col
     * @return index in UF 1D data structure
     *//*
    private int xyTo1D(int row, int col) {
        if (argumentIsIllegal(row) || argumentIsIllegal(col)) return -1;

        // To address n-by-n 2D grid to 1D aray, we need a space equal to n*n
        // plus one place before it for virtual top plus one place after
        // grid space for virtual bottom.
        return virtualTop1Dindex + (row - 1) * gridSize + col;
    }

    *//**
     * Private method for validating indices.
     *
     * @param n An argument for checking
     * @return False if argument is more or equal 1
     *//*
    private boolean argumentIsIllegal(int n) {
        // By convention, the row and column indices are integers between 1
        // and n, so throw java.lang.IllegalArgumentException if any argument
        // is outside its prescribed range.
        if (1 <= n && n <= gridSize) {
            return false;
        } else {
            throw new IllegalArgumentException("By convention, the row/column"
                    + " index should be integer >= 1");
        }
    }

    *//**
     * Return list of grid neighbors in radius = 1
     * @param row
     * @param col
     * @return
     *//*
    private List<PointRowColumn> getNeighbors(int row, int col) {
        List<PointRowColumn> result = new LinkedList<>();

        // add all neighbors in +1 radius
        if (isValidPoint(row + 1, col))
            result.add(new PointRowColumn(row + 1, col));
        if (isValidPoint(row - 1, col))
            result.add(new PointRowColumn(row - 1, col));
        if (isValidPoint(row, col + 1))
            result.add(new PointRowColumn(row, col + 1));
        if (isValidPoint(row, col - 1))
            result.add(new PointRowColumn(row, col - 1));

        return result;
    }

    *//**
     * Checks if point is inside grid bounds
     * @param row
     * @param col
     * @return
     *//*
    private boolean isValidPoint(int row, int col) {
        return (1 <= row
                && row <= gridSize
                && 1 <= col
                && col <= gridSize);
    }

    *//**
     * Private class to wrap point
     *//*
    private static class PointRowColumn {
        int row;
        int col;

        public PointRowColumn(int row, int col) {
            this.row = row;
            this.col = col;
        }
    }
}*/
