import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class PercolationTest {

  @Test
  public void testPercolatesWithZeroArgument() throws Exception {
    try {
      Percolation p = new Percolation(0);
      p.percolates();
      fail("a method didn't throw IllegalArgumentException when I expected it to");
    } catch (IllegalArgumentException e) {
    }
  }

  @Test
  public void testPercolatesWithNegativeArgument() throws Exception {
    try {
      Percolation p = new Percolation(-1);
      p.percolates();
      fail("a method didn't throw IllegalArgumentException when I expected it to");
    } catch (IllegalArgumentException e) {
    }
  }


  @Test
  public void testPercolatesWithPositiveArgument() throws Exception {
    Exception ex = null;

    try {
      Percolation p = new Percolation(1);
      p.percolates();
    } catch (IllegalArgumentException e) {
      ex = e;
      fail("IllegalArgumentException when it's not expected");
    }

    assertEquals(null, ex);
  }
}