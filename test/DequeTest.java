import org.junit.Test;

import java.util.Iterator;
import java.util.NoSuchElementException;

import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DequeTest {
    @Test
    public void testIsEmpty() throws Exception {
        Deque dq = new Deque();
        assertTrue(dq.isEmpty());

        dq.addFirst("first");
        dq.removeFirst();
        assertTrue(dq.isEmpty());

        dq.addLast("last");
        dq.removeLast();
        assertTrue(dq.isEmpty());

        dq.addFirst("first");
        dq.removeLast();
        assertTrue(dq.isEmpty());

        dq.addLast("last");
        dq.removeFirst();
        assertTrue(dq.isEmpty());

        dq.addFirst("first");
        dq.addLast("middle");
        dq.addLast("last");

        dq.removeFirst();
        dq.removeLast();
        dq.removeFirst();
        assertTrue(dq.isEmpty());
    }

    @Test
    public void testSize() throws Exception {
        Deque dq = new Deque();
        dq.addFirst("first");
        assertTrue(dq.size() == 1);

        dq.removeFirst();
        assertTrue(dq.size() == 0);

        dq.addLast("last");
        assertTrue(dq.size() == 1);

        dq.removeLast();
        assertTrue(dq.size() == 0);

        dq.addFirst("first");
        dq.removeLast();
        assertTrue(dq.size() == 0);

        dq.addLast("last");
        dq.removeFirst();
        assertTrue(dq.size() == 0);

        dq.addFirst("first");
        dq.addLast("middle");
        dq.addLast("last");
        assertTrue(dq.size() == 3);

        dq.removeFirst();
        assertTrue(dq.size() == 2);
        dq.removeLast();
        assertTrue(dq.size() == 1);
        dq.removeFirst();
        assertTrue(dq.size() == 0);
    }

/*    @Test
    public void testAddFirst() throws Exception {
        Deque dq = new Deque();
        dq.addFirst("last");
        assertTrue(dq.first.item == "last");
        assertTrue(dq.last.item == "last");
        
        dq.addFirst("middle");
        dq.addFirst("first");

        assertTrue(dq.first.item == "first");
        assertTrue(dq.last.item == "last");
    }

    @Test
    public void testAddLast() throws Exception {
        Deque dq = new Deque();
        dq.addLast("first");
        assertTrue(dq.first.item == "first");
        assertTrue(dq.last.item == "first");

        dq.addLast("middle");
        dq.addLast("last");

        assertTrue(dq.first.item == "first");
        assertTrue(dq.last.item == "last");
    }*/

/*    @Test
    public void testRemoveFirst() throws Exception {
        Deque dq = new Deque();

        dq.addFirst("first");
        dq.removeFirst();
        assertTrue(dq.first == null);

        dq.addFirst("first");
        assertEquals("first", dq.removeFirst());

        dq.addFirst("first");
        dq.addLast("last");
        assertEquals("first", dq.removeFirst());
        assertEquals("last", dq.first.item);
        assertEquals("last", dq.last.item);
    }

    @Test
    public void testRemoveLast() throws Exception {
        Deque dq = new Deque();

        dq.addLast("last");
        dq.removeLast();
        assertTrue(dq.last == null);

        dq.addLast("last");
        assertEquals("last", dq.removeLast());

        dq.addFirst("first");
        dq.addLast("last");
        assertEquals("last", dq.removeLast());
        assertEquals("first", dq.first.item);
        assertEquals("first", dq.last.item);
    }*/

    @Test
    public void testIterator() throws Exception {
        String args = "AA BB BB BB BB BB CC CC";

        Deque<String> dq = new Deque();
        for (String s : args.trim().split(" ")) dq.addLast(s);

        StringBuilder sb = new StringBuilder();

        Iterator itr = dq.iterator();
        assertTrue(itr != null);

        while (itr.hasNext()) sb.append(itr.next() + " ");
        assertTrue(sb.toString().trim().equals(args));

        dq.removeFirst(); dq.removeLast();
        dq.removeFirst(); dq.removeLast();
        dq.removeFirst(); dq.removeLast();
        dq.removeFirst(); dq.removeLast();

        assertTrue(itr != null);
    }

    @Test
    public void testNoSuchElementEx() throws Exception {
        Deque<String> dq = new Deque();
        Exception expectedEx = new NoSuchElementException();

        try {
            dq.removeFirst();
            fail("NoSuchElementException was not thrown when it was expected");
        } catch (Exception e) {
            assertEquals(e.getClass(), expectedEx.getClass());
        }
    }

    @Test
    public void testUnsupportedOperationEx() throws Exception {
        Deque<String> dq = new Deque();
        Exception expectedEx = new UnsupportedOperationException();
        Iterator itr = dq.iterator();

        try {
            itr.remove();
            fail("UnsupportedOperationException was not thrown when it was " +
                    "expected");
        } catch (Exception e) {
            assertEquals(e.getClass(), expectedEx.getClass());
        }
    }

    @Test
    public void testIllegalArgumentEx() throws Exception {
        Deque<String> dq = new Deque();
        Exception expectedEx = new IllegalArgumentException();

        try {
            dq.addLast(null);
            fail("IllegalArgumentException was not thrown when it was " +
                    "expected");

            dq.addFirst(null);
            fail("IllegalArgumentException was not thrown when it was " +
                    "expected");

        } catch (Exception e) {
            assertEquals(e.getClass(), expectedEx.getClass());
        }
    }
}