
import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

public class RandomizedQueueTest {
    RandomizedQueue rq;

    @Before
    public void setup() {
        rq = new RandomizedQueue();
    }

    @Test
    public void testIsEmpty() throws Exception {
        assertEquals(0, rq.size());

        rq.enqueue("first");
        rq.enqueue("second");
        rq.enqueue("third");
        rq.enqueue("forth");
        rq.enqueue("fith");

        rq.dequeue();
        rq.dequeue();
        rq.dequeue();
        rq.dequeue();
        rq.dequeue();

        assertEquals(0, rq.size());
    }

    /*@Test
    public void testSize() throws Exception {
        assertEquals(rq.size(), rq.lastPosition - rq.firstPosition);
    }

    @Test
    public void testEnqueue() throws Exception {
        rq.enqueue("first");
        assertEquals(rq.size(), 1);
        assertEquals(rq.firstPosition, 0);
        assertEquals(rq.lastPosition, 0);

        rq.enqueue("second");
        assertEquals(rq.size(), 2);
        assertEquals(0, rq.firstPosition);
        assertEquals(1, rq.lastPosition);

        rq.enqueue("third");
        assertEquals(rq.size(), 3);
        assertEquals(0, rq.firstPosition);
        assertEquals(2, rq.lastPosition);

        rq.enqueue("forth");
        assertEquals(4, rq.size());
        assertEquals(0, rq.firstPosition);
        assertEquals(3, rq.lastPosition);

        rq.enqueue("fith");
        assertEquals(rq.size(), 5);
        assertEquals(0, rq.firstPosition);
        assertEquals(4, rq.lastPosition);
    }
    */

    @Test
    public void testDequeue() throws Exception {
        int oldSize;

        rq.enqueue("first");
        rq.enqueue("second");
        rq.enqueue("third");
        rq.enqueue("forth");
        rq.enqueue("fith");

        oldSize = rq.size();
        rq.dequeue();
        assertEquals(oldSize - 1, rq.size());

        oldSize = rq.size();
        rq.dequeue();
        assertEquals(oldSize - 1, rq.size());

        oldSize = rq.size();
        rq.dequeue();
        assertEquals(oldSize - 1, rq.size());

        assertEquals("forth", rq.dequeue());

/*        oldSize = rq.size();
        int oldCapacity = rq.capacity;
        rq.dequeue();
        assertEquals(oldSize - 1, rq.size());

        assertTrue(rq.capacity < oldCapacity);*/
    }

    @Test
    public void testSample() throws Exception {
        rq.enqueue("first");
        rq.enqueue("second");
        rq.enqueue("third");
        rq.enqueue("forth");
        rq.enqueue("fith");

        assertNotEquals(rq.sample(), rq.sample());
        assertNotEquals(rq.sample(), rq.sample());
    }

    @Test
    public void testIterator() throws Exception {
        rq.enqueue("first");
        rq.enqueue("second");
        rq.enqueue("third");
        rq.enqueue("forth");
        rq.enqueue("fith");

        Iterator itr = rq.iterator();
        itr.next();
        itr.next();
        itr.next();
        itr.next();
        assertEquals("fith", itr.next());
        assertEquals(false, itr.hasNext());
    }

    @Test
    public void testCoursera() throws Exception {
        assertTrue(rq.isEmpty());   //  ==> true
        rq.enqueue(433);
        rq.dequeue(); //     ==> 433
        assertTrue(rq.isEmpty());   //  ==> true
        assertEquals(0, rq.size()); //        ==> 0
        rq.enqueue(279);
    }

    @Test
    public void testCoursera2() throws Exception {
        rq.enqueue(7);
        rq.enqueue(43);
        rq.enqueue(48);
        rq.enqueue(41);
        rq.enqueue(48);
        rq.dequeue(); //     ==> 7
        rq.enqueue(42);
        rq.enqueue(25);
        rq.enqueue(37);
        assertEquals(7, rq.size()); //         ==> 7
        rq.enqueue(10);
    }


    @Test
    public void testCoursera3() throws Exception {
        rq.enqueue(66);
        rq.enqueue(66);
        rq.dequeue(); //     ==> 66
        rq.dequeue(); //     ==> 66
        rq.enqueue(214);
    }

   /* @Test
    public void testUpsize() throws Exception {
        int sizeBefore = rq.size();
        int capacityBefore = rq.capacity;
        rq.upsize(2);
        int sizeAfter = rq.size();
        int capacityAfter = rq.capacity;

        assertEquals(sizeAfter, sizeBefore);
        assertEquals(capacityAfter, capacityBefore * 2);
    }

    @Test
    public void testDownsize() throws Exception {
        int sizeBefore = rq.size();
        int capacityBefore = rq.capacity;
        rq.downsize(2);
        int sizeAfter = rq.size();
        int capacityAfter = rq.capacity;

        assertEquals(sizeAfter, sizeBefore);
        assertEquals(capacityAfter, capacityBefore / 2);
    }*/
}